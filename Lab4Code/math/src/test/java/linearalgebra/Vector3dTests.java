package linearalgebra;
import org.junit.Test;
import static org.junit.Assert.*;

public class Vector3dTests {

    @Test
    public void test() {
        Vector3d vector = new Vector3d(3, 6, 9);
        assertEquals(3, vector.getX(), 0.00001);
        assertEquals(6, vector.getY(), 0.00001);
        assertEquals(9, vector.getZ(), 0.00001);
    }

    @Test
    public void test2() {
        Vector3d vector = new Vector3d(3, 6, 9);
        assertEquals(Math.sqrt(126), vector.magnitude(), 0.00001);
    }

    @Test
    public void test3() {
        Vector3d vector = new Vector3d(3, 6, 9);
        Vector3d vector2 = new Vector3d(2, 3, 1);
        assertEquals(33, vector.dotProduct(vector2), 0.00001);
    }

    @Test
    public void test4() {
        Vector3d vector = new Vector3d(3, 6, 9);
        Vector3d vector2 = new Vector3d(2, 3, 1);
        Vector3d vector3 = vector.add(vector2);
        assertEquals(5, vector3.getX(), 0.00001);
        assertEquals(9, vector3.getY(), 0.00001);
        assertEquals(10, vector3.getZ(), 0.00001);
    }
}